#!/usr/bin/env node
const express = require('express')
const axios = require('axios');
const app = express()
const port = 3000
const PAYLOAD_SF = 'sf';
const PAYLOAD_DONE = 'done';
const PAYLOAD_TIME = 'time';
sendDownLink()
app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
app.get('/add', function (req, res) {
    var devid = req.param('devid');
    addDevicetoTTN("khellotestapp", "70b3d57ed000ec93", devid, "ec2-" + devid.toLowerCase(), "d5e309385798583fa4ced94f0f1ddb36", null, null, callback, res);
})

var ttn = require('ttn');
function callback(res, message) {
    console.log('calledback')
    res.send(message);
}

function getTime() {
    return time_format(new Date());
}

function getControlData(devID, callback) {
    if (devID.startsWith('ec2-')) devID = devID.substring(4);
    axios.post('https://s5lvzglz4l.execute-api.us-east-2.amazonaws.com/Integration_Dev/control-data', {
        "devEUI": devID
    }).then((res) => {
        let item = res.data.body.items;
        console.log(item);
        let ret = item.daysOff + '' + format_two_digits(item.numberOfHours) + '' + format_two_digits(item.startHour)
            + '' + item.runMode;
        callback(ret);
    }).catch((err) => {
        console.error(err);
    })
}

function sendDownLink() {
    var appID = "khellotestapp"
    var accessKey = "ttn-account-v2.Ngl_1QYDlihAaP6rnr4LPEpAKIQNZOht23_tRHcsfuM"

    var ttn = require('ttn')
    ttn.data(appID, accessKey)
        .then(function (client) {
            client.on("uplink", function (devID, payload) {
                console.log("Received uplink from ", devID)
                console.log(payload)
                let sentMessage = '';
                let payload_raw = payload.payload_raw.toString();
                // console.log('payload_raw', payload_raw);
                if (devID == 'test20dec2018' || devID == 'ec2-0004a30b0021253c' || devID == 'ec2-0004a30b0023fca5' || devID == 'ec2-0004a30b0023ccc1') {
                    if (payload_raw == PAYLOAD_TIME) {
                        sentMessage = getTime();
                        console.log("Sending downlink to", devID, sentMessage);
                        // console.log(sentMessage);
                        // console.log(timeNow.toISOString() + ' ' + formatted_time);
                        client.send(devID, sentMessage + '')
                    }
                    else if (payload_raw == PAYLOAD_SF) {
                        sentMessage = '09';  // fixed for now
                        console.log("Sending downlink to", devID, sentMessage);
                        // console.log('sf', sentMessage);
                        // console.log(timeNow.toISOString() + ' ' + formatted_time);
                        client.send(devID, sentMessage + '')
                    }
                    else { // then it's a payload value
                        // sentMessage = '000000';
                        if (devID == 'ec2-0004a30b0023ccc1') sentMessage = '014120';
                        else sentMessage = '004210';
                        // getControlData(devID, function (sentMessage) {
                        console.log("Sending downlink to", devID, sentMessage);
                        // console.log('Payload value', sentMessage);
                        // console.log(timeNow.toISOString() + ' ' + formatted_time);
                        client.send(devID, sentMessage + '')
                        // });
                    }
                }
                else {
                    if(devID === 'ec2-0850483598315720' || devID === 'ec2-0850483597315220') {
                        let port;
                        if(payload_raw === PAYLOAD_TIME) {
                            port = 2;
                            sentMessage = getTime();
                        }
                        else if(payload_raw === PAYLOAD_SF) {
                            port = 1;
                            sentMessage = '09';
                        }
                        else {
                            port = 3;
                            sentMessage = '004210';
                        }
                        console.log("Sending downlink to", devID, sentMessage);
                        client.send(devID, sentMessage + '', port);
                    }
                    else {
                        sentMessage = getTime();
                        // console.log("Sending downlink to", devID);
                        // console.log(timeNow.toISOString() + ' ' + formatted_time);
                        client.send(devID, sentMessage + '')
                    }
                }
            })
        })
        .catch(function (error) {
            console.error("Error", error)
            process.exit(1)
        })
}

function time_format(d) {
    hours = format_two_digits(d.getHours());
    minutes = format_two_digits(d.getMinutes());
    return hours + '' + minutes;
}

function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
}
function addDevicetoTTN(appId, appEUI, devEUI, devName, appKey, event, context, callback, res) {
    var accessKey = "ttn-account-v2.Ngl_1QYDlihAaP6rnr4LPEpAKIQNZOht23_tRHcsfuM"
    var devID = devName;
    console.log("ADDING", devID);
    var device = {
        "appId": appId,
        "activationMethod": "OTAA",
        // "devId": "",
        "latitude": 0,
        "longitude": 0,
        "altitude": 0,
        "description": "",
        "fCntUp": 0,
        "fCntDown": 0,
        "disableFCntCheck": false,
        "uses32BitFCnt": false,
        // "activationConstraints": "local",
        "usedDevNoncesList": [],
        "usedAppNoncesList": [],
        "lastSeen": 0,
        "appEui": appEUI,
        "devEui": devEUI,
        "appKey": appKey,
        // "appSKey": "d5e309385798583fa4ced94f0f1ddb36",
        // "nwkSKey": "1f447b7d082a857f696b6c2c2af99a9c",
        // "devAddr": "2601165c",
        "attributes": {}
    };

    ttn.application(appId, accessKey)
        .then(function (client) {
            client.registerDevice(devID, device).then(function () {
                console.log("Dev ADDED")
                callback(res, "DEV ADDED")
                return "OK";
            }).catch(function (err) {
                console.log(err)
                callback(res, err)
                return err;
            });
        })
}

module.exports = { addDevicetoTTN };
